#!/usr/bin/python

import sys

def process(x, y, indices):
    data = eval(open('{}x{}'.format(x, y)).read())
    f = open('{}x{}.out'.format(x, y), 'w')
    area = float(x*y)
    f.write('# Mines Density Samples  <-->  average      median     std-dev         min         max   avg/median ratio\n\n')
    for index in indices:
        try:
            times = data[index]
        except IndexError:
            break
        if len(times) <= 1:
            dashes = '-'.join([' '*x for x in [14, 11, 11, 11, 11, 3, 0]])
            f.write('#{:=6} {:=6.2f}%   {:=5}{}\n'.format(index, 100.*index/area, len(times), dashes))
        else:
            average = sum(times)/len(times)
            if len(times) % 2:
                median = times[len(times)//2]
            else:
                median = (times[len(times)//2 - 1] + times[len(times)//2]) / 2
            standard_deviation = ((sum([(time - average)**2 for time in times]))/(len(times) - 1))**.5
            lowest = times[0]
            highest = times[-1]
            f.write('{:=7} {:=6.2f}%   {:=5}    {:=10.3f}s {:=10.3f}s {:=10.3f}s {:=10.3f}s {:=10.3f}s  {:=6.3f}\n'.format(
                index, 100.*index/area, len(times), average, median, standard_deviation, lowest, highest, average/median
            ))
    f.close()

def old_main():
    for pair in sys.argv[1:]:
        process(*map(int, pair.split('x')))

def main():
    process(20, 20, range(109))
    process(50, 50, range(250, 626, 5))
    process(10, 10, range(0, 36) + range(80, 97))

if __name__ == '__main__':
    main()

