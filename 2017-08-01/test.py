#!/usr/bin/python

import anonymine_engine
import pprint
import random
import sys
import time


def init_time(width, height, n_mines, runs=10, cfgfile='testcfg'):
    i = 0
    times = []
    while i < runs:
        i += 1
        e = anonymine_engine.game_engine(
            cfgfile,
            width=width, height=height, mines=n_mines
        )
        coord = random.randint(0, width - 1), random.randint(0, height - 1)
        start = time.time()
        try:
            e.init_field((coord))
        except anonymine_engine.security_alert as e:
            sys.stderr.write('\n{}\n'.format(e.message))
        x = time.time() - start
        times.append(x)
        times.sort()
        msg = '{}: {}  This: {}  Avg: {}  Median: {}  Min/Max: {}/{}'.format(
            '{}@{}x{}'.format(n_mines, width, height),
            '{}/{}'.format(i, runs),
            x,
            sum(times)/len(times),
            times[len(times)//2],
            times[0], times[-1],
        )
        sys.stderr.write('\x1b[2K\r')
        sys.stderr.write(msg)
        sys.stderr.flush()
    sys.stderr.write('\n')
    return times


def init_time_curve(x, y, min_mines, max_mines, step, runs, cfgfile='testcfg'):
    try:
        data = eval(open('{}x{}'.format(x, y)).read())
    except:
        data = []
    if min_mines > len(data):
        data.extend([[]] * (min_mines-len(data)))
    if len(data) > min_mines:
        start = len(data)        
    else:
        start = min_mines
    for i in range(start, max_mines + step, step):
        datum = init_time(x, y, i, runs, cfgfile=cfgfile)
        data.append(datum)
        data.extend([[]] * (step-1))
        f = open('{}x{}'.format(x, y), 'w')
        f.write(pprint.pformat(data) + '\n')
        f.close()

if __name__ == '__main__':
    # Step MUST be 1 for single density runs
    #               Size    Mines       Step    Runs
    init_time_curve(20, 20, 0, 99,      1,      100)
    init_time_curve(20, 20, 100, 104,   2,      50)
    init_time_curve(20, 20, 108, 108,   1,      5)
    init_time_curve(50, 50, 250, 500,   5,      50)
    init_time_curve(50, 50, 525, 551,   25,     25)
    init_time_curve(50, 50, 575, 575,   1,      2)
    init_time_curve(10, 10, 0, 25,      1,      100)
    init_time_curve(10, 10, 26, 29,     1,      25,     'testcfg-2h')
    init_time_curve(10, 10, 30, 33,     1,      10,     'testcfg-2h')
    init_time_curve(10, 10, 84, 86,     1,      5,      'testcfg-2h')
    init_time_curve(10, 10, 87, 90,     1,      25,     'testcfg-15min')
    init_time_curve(10, 10, 91, 96,     1,      25,     'testcfg-15min')
