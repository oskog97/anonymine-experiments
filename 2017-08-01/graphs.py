#!/usr/bin/python

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy
import math

import matplotlib.ticker as mtick
def ticks(y, pos):
    return r'$e^{' + '{:.0f}'.format(numpy.log(y)) + '}$'
    
def main(size):
    fig = Figure()
    canvas = FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.semilogy(basey=numpy.e)
    ax.yaxis.set_major_formatter(mtick.FuncFormatter(ticks))
    ax.set_title(size)
    ax.set_xlabel('Density (%)')
    ax.set_ylabel('Initialization time (s) (log)')

    a, b = map(float, map(int, size.split('x')))
    area = a*b
    
    x = []
    y_median = []
    y_average = []
    y_std = []
    y_samples = []
    for index, datum in enumerate(eval(open(size).read())):
        if len(datum) > 1:
            x.append(100. * index / area)
            y_median.append(numpy.median(datum))
            y_average.append(numpy.average(datum))
            y_std.append(numpy.std(datum))
            y_samples.append(len(datum))
            ax.plot([x[-1]] * len(datum), datum,
                linestyle='None', marker='_', color='0.33')
    
    # Calculate text placement
    lowest = math.log(min(y_median + y_average + y_std))
    highest = math.log(max(y_median + y_average + y_std + y_samples))
    def y_position(n):
        return math.e**(lowest + n*(highest - lowest))
    x_position = x[0] + 0.05*(x[-1] - x[0])
    
    ax.text(x_position, y_position(.96), 'red = median, cyan = average')
    ax.text(x_position, y_position(.88), 'blue = standard deviation')
    ax.text(x_position, y_position(.80),
        'magenta = samples {}'.format(sorted(set(y_samples), reverse=True)))
    ax.text(x_position, y_position(.72), 'grey = sample times')
    
    ax.plot(x, y_samples, 'm.')
    ax.plot(x, y_std, 'b.-')
    ax.plot(x, y_average, 'c.-')
    ax.plot(x, y_median, 'r.-')
    
    fig.set_dpi(180)
    canvas.print_png(size + '.png')

def percentiles(size):
    #which_percentiles = [1,  5,  10, 25, 33, 50, 67, 75, 90, 95, 99]
    #which_percentiles = [10, 20, 30, 40, 50, 60, 70, 80, 90]
    #which_percentiles = [1,  10, 33, 50, 67, 90, 99]
    which_percentiles =  [1,  99, 10, 90, 33, 67, 50]
    fig = Figure()
    canvas = FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.semilogy(basey=numpy.e)
    ax.yaxis.set_major_formatter(mtick.FuncFormatter(ticks))
    ax.set_title(size + ' (various percentiles)')
    ax.set_xlabel('Density (%)')
    ax.set_ylabel('Initialization time (s) (log)')

    a, b = map(float, map(int, size.split('x')))
    area = a*b
    
    x = []
    y = [[] for _ in which_percentiles]
    samples = []
    for index, datum in enumerate(eval(open(size).read())):
        if len(datum) > 1:
            x.append(100. * index / area)
            samples.append(len(datum))
            ax.plot([x[-1]] * len(datum), datum, 'r_')
            for y_index, percentile in enumerate(which_percentiles):
                y[y_index].append(numpy.percentile(datum, percentile))
    
    # Calculate text placement
    lowest = math.log(min(map(min, y)))
    highest = math.log(max(map(max, y) + samples))
    def y_position(n):
        return math.e**(lowest + n*(highest - lowest))
    x_position = x[0] + 0.05*(x[-1] - x[0])
    
    ax.text(x_position, y_position(.96),
        'greyscale = percentiles {}'.format(sorted(which_percentiles)))
    ax.text(x_position, y_position(.88), 'red = sample times')
    ax.text(x_position, y_position(.80),
        'blue = samples {}'.format(sorted(set(samples), reverse=True)))
    
    ax.plot(x, samples, 'b.')
    for y_index in range(len(which_percentiles)):
        ax.plot(x, y[y_index], marker='.',
            color=str(.75-.75*y_index/float(len(which_percentiles) - 1)))
    
    fig.set_dpi(180)
    canvas.print_png(size + '-percentiles.png')


if __name__ == '__main__':
    main('10x10')
    main('20x20')
    main('50x50')
    percentiles('10x10')
    percentiles('20x20')
    percentiles('50x50')
