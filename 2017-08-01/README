There appears to be a knee at around 23%, this needs to be investigated.


Analysis of the data
====================

    At around 15...20 % the median time suddenly starts growing exponentially.
    At lower densities, it appears to be weakly polynomial (linear or square).
    
    Other percentiles start going exponential at other points.
    Two examples to clarify the "polarity" of the percentiles, and to clarify
    to the layman what a "percentile" is:
        - There's a 10% chance that the field will be initialized in a time at
          most the value of the 10th percentile is.
        - The time taken to initialize is shorter than the 90th percentile in
          90% of all cases.
        - There's a n% chance that the field will be initialized in a time at
          most the value of the n:th percentile is.
    
    NOTE: Lower percentiles go exponential later than higher percentiles.
    Remembering this will make the graphs easier to read.
        
    The standard deviation (randomness/variation) grows exponentially.
    
    There's some funny stuff going on which can be seen in the graphs.


Files
=====

    Graphs
    ------
        10x10-percentiles.png
        10x10.png
        20x20-percentiles.png
        20x20.png
        50x50-percentiles.png
        50x50.png
    
    Raw data
    --------
        10x10               Removed the extreme end to clean the graph
        10x10.original
        20x20
        50x50

    Text summaries of the data
    --------------------------
        10x10.out 10x10.out.orig
        20x20.out
        50x50.out
    
    Setup
    -----
        README              See the next section "Setup"
        testcfg
        testcfg-15min
        testcfg-2h
    
    Tools
    -----
        test.py             Data collector
        graphs.py
        process.py          Writes the text summaries



Setup
=====
    
    Worker processes    2
    
    Field size: 20x20, i.e. the default
        Density:    0 ... 24.75%,       .25% increments
                    0 ... 99 mines,     1 mine increments
        Runs:       100 per density
        Timeout:    14400
        
        Density:    25 ... 26%,         .5% increments
                    100 ... 104 mines,  2 mines increments
        Runs:       50 per density
        Timeout:    14400
        
        Density:    27%, 108 mines
        Runs:       5
        Timeout:    14400
    
    Field size 50x50
        Density:    10 ... 20%,         .2% increments
                    250 ... 500 mines,  5 mines increments
        Runs:       50 per density
        Timeout:    14400
        
        Density:    21 ... 22%,         1% increments
                    525 ... 550 mines,  25 mines increments
        Runs:       25 per density
        Timeout:    14400
        
        Density:    23%, 575 mines
        Runs:       2
        Timeout:    14400
    
    Field size 10x10,  1% = 1 mine
        Density:    0 ... 25,           1 increments
        Runs:       100
        
        Density:    26 ... 29           1 increments
        Runs:       25
        Timeout:    7200
        
        Density:    30 ... 33           1 increments
        Runs:       10
        Timeout:    7200
        
        Density:    84 ... 86           1 increments
        Runs:       5
        Timeout:    7200
        
        Density:    87 ... 90           1 increments
        Runs:       25
        Timeout:    900
        
        Density:    91 ... 96           1 increments
        Runs:       25
        Timeout:    900

