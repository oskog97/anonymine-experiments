#!/usr/bin/env python

import os
import platform
import random
import signal
import socket
import sys
import time
import traceback

import anonymine_fields
import anonymine_solver

def pypy_postmortem_marker(*args):
    pass

# Local file socket_textio.py
from socket_textio import EOF, readline, send


class ConnectionError(Exception):
    pass


try:
    cfg = eval(open('client.cfg').read())
except RuntimeError:
    raise RuntimeError('Cannot import configuration from client.cfg')


# Global array to carry data to be printed by siginfo_handler:
siginfo_data = [None, None, None, None]
siginfo_data_procinfo = 0       # Fixed length string (always 16)
siginfo_data_runcounter = 1     # Variable length integer
siginfo_data_runs = 2           # Constant length integer (constant per process)
siginfo_data_task = 3           # Variable length string

def siginfo_handler(*args):
    n_digits = len(str(siginfo_data[siginfo_data_runs]))
    print(('{}: {:%d}/{}: {}'%n_digits).format(*siginfo_data))


def performance_is_accurate():
    '''
    Returns True if time data will be accurate.
    '''
    if 'freqcheckcmd' in cfg:
        return os.system(cfg['freqcheckcmd']) == 0
    else:
        return False


def python_name():
    '''
    Return 'CPython 2', 'CPython 3', 'PyPy 2', or 'PyPy 3'
    '''
    return platform.python_implementation() + ' ' + str(sys.version_info[0])


def test(task):
    '''
    success, rule9bf_useful = test(category_string)
    '''
    pypy_postmortem_marker('<client> task', task)

    mines, rest = task.split('@')
    n_mines = int(mines)
    rest, field_type = rest.split('-')
    size = list(map(int, rest.split('x')))

    if field_type == 'moore':
        field = anonymine_fields.generic_field(size)
    elif field_type == 'neumann':
        field = anonymine_fields.generic_field(size, False)
    elif field_type == 'hex':
        field = anonymine_fields.hexagonal_field(size[0], size[1])

    # Place mines and reveal a random safe cell.
    m = n_mines
    mines = field.all_cells()
    random.shuffle(mines)
    field.fill(mines[:m])
    for mine in mines[m:]:
        for neighbour in field.get_neighbours(mine):
            if neighbour in mines[:m]:
                break
        else:
            field.reveal(mine)
            break

    # Set up solver, limit rule9bf the same way as in the game
    solver = anonymine_solver.solver()
    solver.field = field
    solver.rule9bf_max = 13
    solver.rule9bf_action = lambda x: True

    success, difficulty = solver.solve()

    if -2 in difficulty:
        rule9bf_used = difficulty[-2] > 0
    else:
        rule9bf_used = False

    return success, rule9bf_used and success


def single_sample(connection, time_diffs=[]):
    '''
    Ask the server for a single task and complete it and send the
    results back.

    Call this function in a loop?
    '''
    # Check load average both before and after trying to solve the minefield.
    use_time = performance_is_accurate()

    if len(sys.argv) == 3:
        # pypy client.py <runs> <category>
        task = sys.argv[2]
    else:
        # Normal operation, ask for a task.
        try:
            send(connection, 'get-task\n')
            task = readline(connection)
        except:
            # Raise an exception and let the caller create a new connection.
            raise ConnectionError
        if task.lower().startswith('error'):
            sys.stderr.write('Server returned: {}\n'.format(task))
            sys.exit(1)
        if task == 'idle':
            return

    global siginfo_data
    siginfo_data[siginfo_data_task] = task

    try:
        # os.times().user + os.times().system       # Why?
        start_time = sum(os.times()[:2])
        success, rule9bf_useful = test(task)
        time_taken = sum(os.times()[:2]) - start_time
    except KeyboardInterrupt:
        raise
    except:
        exception = sys.exc_info()
        traceback.print_exception(*exception)
        os.abort()

    # New time take reports 3.7 +/- 19.0 ms faster.
    # Tested with 1000 samples on Xeon E3 1225-v3, low load, nice 0
    # Minimum:              -257.811 ms
    # Avegage:                -3.678 ms
    # Maximum:               +17.057 ms
    # Median:                 -1.399 ms
    # Standard deviation:     18.966 ms
    # Cumulative:          -3677.587 ms

    if False:
        print('{:+.3f} ms = {:+.2f}%'.format(
            (time_taken - old_time_taken) * 1000,
            ((time_taken/old_time_taken)-1) * 100
        ))
    if False:
        time_diffs.append((time_taken - old_time_taken) * 1000)
        avg = sum(time_diffs)/len(time_diffs)
        print('min {:.3f} ms, avg {:.3f} ms, max {:.3f} ms, '
            'median {:.3f} ms, stddev {:.3f} ms, cum {:.3f} ms'.format(
                min(time_diffs),
                avg,
                max(time_diffs),
                sorted(time_diffs)[len(time_diffs)//2],
                (sum([(x-avg)**2 for x in time_diffs])/len(time_diffs))**.5,
                sum(time_diffs),
        ))

    # Check load average again
    use_time = use_time and performance_is_accurate()

    boolean = {True: 'yes', False: 'no'}
    message = 'publish {}\n'.format(task)
    message += 'success: {}\n'.format(boolean[success])
    message += 'rule9bf-useful: {}\n'.format(boolean[rule9bf_useful])
    if use_time:
        message += 'time: {}\n'.format(time_taken)
        message += 'python: {}\n'.format(python_name())
        message += 'machine: {}\n'.format(cfg['machine'])
    message += 'end\n'

    try:
        send(connection, message)
    except:
        close_connection(connection)
        connection = create_connection()
        send(connection, message)
        # Caller's connection is still broken.  Will raise an exception
        # next iteration after sending 'get-task\n' to the server.


def create_connection():
    '''
    Create connection. Tries again until it succeeds.
    '''
    while True:
        try:
            connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            connection.connect((cfg['host'], cfg['port']))
            send(connection, cfg['worker-pass'] + '\n')
            return connection
        except:
            close_connection(connection)
            time.sleep(1)


def close_connection(connection):
    '''
    Close connection properly. Never raises an exception.
    '''
    try:
        send(connection, 'return\nexit\n')
    except:
        pass
    try:
        connection.shutdown(0)
        connection.close(0)
    except:
        pass


def client():
    '''
    Main function
    '''

    run_counter = 0
    if len(sys.argv) == 3:
        # pypy client.py <runs> <category>
        runs = int(sys.argv[1])
    else:
        runs = cfg['runs']

    global siginfo_data
    siginfo_data[siginfo_data_procinfo] = '{:9} {:6}'.format(
        python_name(),
        os.getpid()
    )
    siginfo_data[siginfo_data_runs] = runs

    # Let CPU cool down if needed
    try:
        pid = os.fork()
    except:
        pid = None
    if pid == 0:
        def kill_parent(signal):
            parent = os.getppid()
            if parent == 1:
                exit(0)
            os.kill(parent, signal)
        try:
            while True:
                if os.system(cfg['stallcmd']) == 0:
                    kill_parent(signal.SIGSTOP)
                time.sleep(cfg['stalltime'])
                kill_parent(signal.SIGCONT)
        except KeyboardInterrupt:
            pass

    # Do work
    connection = create_connection()
    try:
        while runs is None or runs > run_counter:
            run_counter += 1
            siginfo_data[siginfo_data_runcounter] = run_counter
            pypy_postmortem_marker('<client> Run counter', run_counter)
            # Single sample
            try:
                single_sample(connection)
            except ConnectionError:
                # Raised before any work could begin.
                run_counter -= 1
                close_connection(connection)
                connection = create_connection()
            if pid is None:
                # Let CPU cool down if needed (non-unix)
                while os.system(cfg['stallcmd']) == 0:
                    time.sleep(cfg['stalltime'])
    except KeyboardInterrupt:
        pass
    close_connection(connection)

    if pid is not None:
        os.kill(pid, signal.SIGTERM)



if __name__ == '__main__':
    try:
        signal.signal(signal.SIGUSR1, siginfo_handler)
        signal.signal(signal.SIGINFO, siginfo_handler)      # BSD only
    except:
        pass
    client()
