#!/usr/bin/env python3

from __future__ import print_function

import cgitb
cgitb.enable()

import math
import os
import pprint
import socket

# Local file socket_textio.py
from socket_textio import *

try:
    cfg = eval(open('web.cfg').read())
except:
    raise RuntimeError('Cannot import configuration from web.cfg')


def connect():
    '''
    Create a connection to the server ready for use.
    Already authenticated as viewer.
    '''
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.connect((cfg['host'], cfg['port']))
    send(connection, 'view\n')
    return connection

def submit(connection):
    '''
    Cause the server to close the connection after having sent all data.

    Use this to get an EOF exception from readline.
    '''
    send(connection, 'return\nexit\n')

def close(connection):
    '''
    Close connection.
    '''
    connection.shutdown(0)
    connection.close()


def preamble(title):
    '''
    Call this to begin output.  This writes the HTTP headers.
    Also starts the HTML document and adds a title and <h1>.
    '''
    print('Content-Type: text/html; charset=utf-8')
    print('Cache-Control: no-cache')
    print('')
    print('<!DOCTYPE html>')
    print('<html xmlns="http://www.w3.org/1999/xhtml" lang="en">')
    print('''
        <head>
            <meta charset="utf-8"/>
            <title>{0}</title>
            <style>
                /* Bold ~ indistinguishable from normal ~ */
                b {{
                    font-size: 120%;
                }}

                /* Probability pages and status page */
                table {{
                    border-collapse: collapse;
                }}
                td {{
                    border: 1px solid;  /* Probablity pages override this. */
                }}

                /* Status page nested tables */
                .status-row table td {{
                    border-top: 0px;
                }}
                .status-row caption {{
                    border-bottom: 2px solid;
                }}

                /* Probability pages */
                table.probability {{
                    font-size: 90%;
                    margin-top: 1em;
                    margin-bottom: 1em;
                }}
                .gamma a {{
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }}
                .probability td {{
                    padding-left: 2px;
                    padding-right: 2px;
                    padding-top: 5px;
                    padding-bottom: 5px;
                    font-size: 90%;
                    border: 1px solid #4444cc;
                }}
                .probability a:link, .probability a:visited {{
                    color: #000000;
                }}
                .probability th {{
                    font-size: 70%;
                }}
                .low-total {{
                    background-color: #ffff55;
                    color: #000000;
                }}
                .canceled {{
                    background-color: #ffaa00;
                    font-style: italic;
                }}
                .KeyError {{
                    background-color: #ff0000;
                    font-weight: bold;
                    color: #000000;
                }}
                .sweep {{
                    background-color: #cccccc;
                    color: #000000;
                }}
            </style>
        </head>
        <body>
        <h1>{0}</h1>'''.format(title)
    )

def end_document():
    '''
    Print a footer and end the HTML document
    '''
    print('''
        <div id="navigation">
        <hr/>
        <p><a href="web.py?probability-moore">Table for Moore</a></p>
        <p><a href="web.py?probability-neumann">Table for Neumann</a></p>
        <p><a href="web.py?probability-hex">Table for hexagonal</a></p>
        <p><a href="web.py?status">Status/progress</a></p>
        </div>
        </body>
        </html>
    ''')


def query_stat(match):
    '''
    Run the 'stat' command on the server, with `match` as argument.
    Without argument if `match` is None.

    Returns:
    {
        category: {
            'target-success': <int>,
            'target-total': <int>,
            'success': <int>,
            'total': <int>,
        }
    }
    '''
    connection = connect()
    send(connection, 'lines all\n')
    if match is not None:
        send(connection, 'stat %s\n' % match)
    else:
        send(connection, 'stat\n')
    submit(connection)
    data = {}
    try:
        while True:
            line = readline(connection)
            # Normalize spaces for parsing
            line = line.replace('\t', ' ')
            while "  " in line:
                line = line.replace("  ", " ")
            # Parse line
            # <category> <target-success> <target-total>: <success>/<total>, ...
            category, min_success, max_total, ratio, _ = line.split(' ', 4)
            max_total = max_total.rstrip(':')
            ratio = ratio.rstrip(',')
            success, total = ratio.split('/')
            min_success, max_total, success, total = map(
                int,
                (min_success, max_total, success, total)
            )
            # Add entry to data
            data[category] = {
                'target-success': min_success,
                'target-total': max_total,
                'success': success,
                'total': total,
            }
    except EOF:
        pass
    close(connection)
    return data


def percentage(numerator, denominator):
    '''
    Turn ratio numerator/denominator into a percentage as a string
    with reasonable precision.

    Example return values:
        '3/5'
        '~20%'
        '1.23%'
        '<b>~</b>10%'       Bold ~ instead of ~~

    BUGS:
        Yes.
    '''
    if numerator < 2:
        return '{}/{}'.format(numerator, denominator)

    # Prevent math domain error when called from the status page
    if numerator > denominator:
        if denominator != 0:
            return '{:.2f}%'.format(100.*numerator/denominator)
        else:
            return ''

    # +0.1 to fix rounding issue with log(1000, 10)
    precision = int(math.log(min(numerator, denominator-numerator)+0.1, 10))
    if precision == 0:
        if denominator > 20:
            if numerator != denominator:
                value = '~' + percentage(10*numerator, 10*denominator)
            else:
                value = '~~100%'
            value = value.replace('~~', '<b>~</b>')
            return value
        else:
            return '{}/{}'.format(numerator, denominator)

    ratio = float(numerator)/denominator
    magnitude = int(math.floor(math.log(ratio, 10)))

    # log(100) is 2, +1 due to precision being 1 too great
    decimals = -magnitude + precision - 3
    if decimals < 0:
        prefix = '~'
        decimals = 0
        # Assume decimals = -1
        # Truncate the -1th decimal
        ratio = int(ratio*10 + 0.5)/10.
    else:
        prefix = ''

    number = ('{:.%df}'%decimals).format(100*ratio)

    # 0.095 with 1 significant digits becomes 0.10, strip trailing zeroes
    # if needed.  Same goes for 9.5% to 10%
    out_precision = len(number) - len(number.rstrip('0')) + 1
    if out_precision > precision:
        if '.' in number:
            number = number[:-1]
        else:
            prefix = '~'

    return prefix + number + '%'


def probability_table(field_type):
    '''
    Create probability table for `field_type`.
    `field_type` in ('moore', 'neumann', 'hex')
    '''
    def first_only(iterable):
        '''
        Return the first part of a list of tuples
        example: [(0, 1), (2, 3)] -> [0, 2]
        '''
        return [x[0] for x in iterable]

    # Seperate gamme selection form field type
    if ';' in field_type:
        field_type, gamma = field_type.split(';')
        gamma = float(gamma)
    else:
        gamma = 0.5

    preamble('Probability table for ' + field_type + ' fields')
    data = query_stat(field_type)
    # tasks.py in local directory
    from tasks import sizes,densities,density_sweep_sizes,size_sweep_densities

    # Gamma selection ... continued
    print('<p class="gamma">Gamma: ')
    for value in (3, 2, 1, 0.50, 0.33, 0.25, 0.20):
        print('<a href="web.py?probability-{0};{1}">{2}{1}{3}</a>'.format(
            field_type, value, '['*(value==gamma), ']'*(value==gamma)
        ))
    print('</p>')
    print('<p>Note that there may be tasks/categories not visible on')
    print('the tables.</p>')

    # Used by both main table and aspect ratio table to print the cells:
    def print_cell(category):
        if category not in data:
            print('<td class="KeyError">X</td>')
            return
        # Get data:
        success = data[category]['success']
        total = data[category]['total']
        target_success = data[category]['target-success']
        target_total = data[category]['target-total']
        # Link to the info page
        URL = "web.py?" + category
        # Canceled?
        if target_success == 0 or target_total == 0:
            print('<td class="canceled"><a href="{}">{}</a></td>'.format(
                URL, percentage(success, total)
            ))
            return
        # Not canceled
        if min(success, total) < 2:
            # Very few successes
            print(
                '<td class="low-total"><a href="{}">'
                '{}/{}</a></td>'.format(URL, success, total))
        else:
            ratio = float(success)/total
            # Calculate colors:
            color = ratio**gamma
            color_byte = int(255.5 * color)
            bgcolor = '#{0:02x}{0:02x}{0:02x}'.format(color_byte)
            fgcolor = '#ffffff' if color < 0.5 else '#000000'
            print(
                '<td style="background-color: {};">'
                '<a style="color: {};" href="{}">'
                '{}</a></td>'.format(
                    bgcolor, fgcolor, URL, percentage(success, total)
            ))

    # Main table
    print('<table class="probability">')
    print('<caption>Probability of a {} minefield being'.format(field_type))
    print('solveable by mine density and field size:</caption>')
    # Top header
    print('<tr><th></th>')
    for density in densities:
        if density in first_only(size_sweep_densities):
            klass = 'class="sweep"'
        else:
            klass = ""
        print('<th {}>{:.1f}%</th>'.format(klass, density))
    print('</tr>')
    # Side header and body
    for size in sizes:
        if size in first_only(density_sweep_sizes):
            klass = 'class="sweep"'
        else:
            klass = ""
        print('<tr><th {0}>{1}x{1}</th>'.format(klass, size))
        for density in densities:
            # Make category string
            # Use `size * size` instead of `size**2` due to rounding errors
            mines = int(0.5 + density/100. * size * size)
            category = '{0}@{1}x{1}-{2}'.format(mines, size, field_type)
            print_cell(category)
        print('</tr>')
    print('</table>')


    print('<table><tr><td style="border: none;">')

    # Aspect ratio table
    from tasks import ratios, ratio_sizes, ratio_densities
    print('<table class="probability">')
    print('<caption>Aspect ratio sweeps</caption>')
    # Top header
    print('<tr><th></th>')
    for ratio in ratios:
        print('<th>{}</th>'.format(ratio))
    print('</tr>')
    # Side header and body
    for size in ratio_sizes:
        for density in ratio_densities:
            print('<tr><th>{}% @ {}<sup>2</sup></th>'.format(density, size))
            for ratio in ratios:
                x = int(size*ratio**.5 + .5)
                y = int(size/ratio**.5 + .5)
                mines = int(0.5 + density/100. * x*y)
                category = '{}@{}x{}-{}'.format(mines, x, y, field_type)
                print_cell(category)
            print('</tr>')
        # Repeat top header
        print('<tr><th></th>')
        for ratio in ratios:
            print('<th>{}</th>'.format(ratio))
        print('</tr>')
    print('</table>')

    print('</td><td style="padding-left: 2em; border: none;">')

    # NOTE: This is copy-pasted from above and just has the loops swapped around
    # Aspect ratio table
    #from tasks import ratios
    print('<table class="probability">')
    print('<caption>Aspect ratio sweeps</caption>')
    # Top header
    print('<tr><th></th>')
    for ratio in ratios:
        print('<th>{}</th>'.format(ratio))
    print('</tr>')
    # Side header and body
    for density in ratio_densities:
        for size in ratio_sizes:
            print('<tr><th>{}% @ {}<sup>2</sup></th>'.format(density, size))
            for ratio in ratios:
                x = int(size*ratio**.5 + .5)
                y = int(size/ratio**.5 + .5)
                mines = int(0.5 + density/100. * x*y)
                category = '{}@{}x{}-{}'.format(mines, x, y, field_type)
                print_cell(category)
            print('</tr>')
        # Repeat top header
        print('<tr><th></th>')
        for ratio in ratios:
            print('<th>{}</th>'.format(ratio))
        print('</tr>')
    print('</table>')

    print('</td></tr></table>')

    end_document()


def info(category):
    '''
    Create status page for specific "category".
    Also includes time.
    '''
    preamble('Info for ' + category)

    print('<h2>Status</h2>')
    statline = query_stat(category)[category]
    print('<p>{}/{} = {}, target: {}, stop denominator: {}</p>'.format(
        statline['success'],
        statline['total'],
        percentage(statline['success'], statline['total']),
        statline['target-success'],
        statline['target-total'],
    ))

    connection = connect()
    send(connection, 'time %s\n' % category)
    submit(connection)
    print('<h2>Time</h2>')
    print('<p>Average time in seconds.')
    print('Followed by number of samples in parenthesis.</p>')
    print('<pre>', end='')
    try:
        while True:
            print(readline(connection))
    except EOF:
        pass
    print('</pre>')
    close(connection)

    try:
        content = open('BENCHMARK').read()
        print('<h2>Benchmark info</h2>')
        print('<pre>{}</pre>'.format(content))
    except:
        pass

    end_document()


def status():
    '''
    Create overall status/progress page
    '''
    def category_parser(category):
        '''
        Function used for sorting categories/tasks in a natural way.

        field_type, height, width, mines = category_parser(string)
        '''
        rest, field = category.split('-')
        mines, size = rest.split('@')
        width, height = size.split('x')
        mines, width, height = map(int, (mines, width, height))
        return (field, height, width, mines)

    def percentage(a, b):
        '''
        status() should call this 'percentage' function instead.
        This one mesures progress down to 1 ppm.  (The maginutude of
        numerator and (denominator-numerator) are NOT used to limit
        precision.)
        '''
        if b != 0:
            return '{:.4f}%'.format(100.*a/b)
        else:
            return ''

    preamble('Status/progress')
    print('<p><a href="#navigation">To navigation</a></p>')

    unfiltered_data = query_stat(None)
    # Separate data into four piles depending on status
    # and also calculate summary
    summary = {
        'success': 0, 'target-success': 0, 'total': 0, 'target-total': 0,
    }
    working_pile = []
    finished_pile = []
    gave_up_pile = []
    canceled_pile = []
    piles = [working_pile, finished_pile, gave_up_pile, canceled_pile]
    for category in sorted(unfiltered_data, key=category_parser):
        datum = unfiltered_data[category]
        datum['success']=min(datum['success'], datum['total']-datum['success'])
        # Summary:
        for key in summary:
            summary[key] += datum[key]
        # Split into four piles:
        if datum['target-success'] == 0 or datum['target-total'] == 0:
            canceled_pile.append((category, datum))
        elif datum['success'] >= datum['target-success']:
            finished_pile.append((category, datum))
        elif datum['total'] >= datum['target-total']:
            gave_up_pile.append((category, datum))
        else:
            working_pile.append((category, datum))

    # Print summary table
    print('<table><caption>Summary of ALL:</caption>')
    print('<tr><th>Success</th><td>{}</td><td>{}</td><td>{}</td></tr>'.format(
        summary['success'], summary['target-success'],
        percentage(summary['success'], summary['target-success'])
    ))
    print('<tr><th>Total</th><td>{}</td><td>{}</td><td>{}</td></tr>'.format(
        summary['total'], summary['target-total'],
        percentage(summary['total'], summary['target-total'])
    ))
    print('</table>')

    # Print something that can be grepped for measuring the rate of tasks
    # being completed:
    print('<!--')
    print('TOTAL:{}'.format(summary['total']))
    print('-->')

    # Print main table
    rows = max(map(len, piles))
    print('<table><caption>Tasks by completion status</caption><tr>')
    print('<th>Working on ({})</th>'.format(len(working_pile)))
    print('<th>Finished ({})</th>'.format(len(finished_pile)))
    print('<th>Too hard ({})</th>'.format(len(gave_up_pile)))
    print('<th>Canceled ({})</th>'.format(len(canceled_pile)))
    print('</tr>')

    # Count progress by pile.  Uses nested tables.
    print('<tr class="status-row">')
    for pile in piles:
        data = {
            'success': 0, 'target-success': 0, 'total': 0, 'target-total': 0,
        }
        for ignore, item in pile:
            for key in data:
                data[key] += item[key]
        print('<td><table><caption>Columnwise summary:</caption>')
        print('<tr><th>Success</th><td>{}</td><td>{}</td>'.format(
            data['success'], data['target-success']
        ))
        print('<td>{}</td></tr>'.format(
            percentage(data['success'], data['target-success'])
        ))
        print('<tr><th>Total</th><td>{}</td><td>{}</td>'.format(
            data['total'], data['target-total']
        ))
        print('<td>{}</td></tr>'.format(
            percentage(data['total'], data['target-total'])
        ))
        print('</table></td>')
    print('</tr>')

    # Print all categories:
    for row in range(rows):
        print('<tr>')
        for pile in piles:
            try:
                data = pile[row][1]
                if pile == canceled_pile:
                    print('<td><a href="web.py?{0}">{0}</a></td>'.format(
                        pile[row][0]
                    ))
                else:
                    print(
                        '<td><a href="web.py?{0}">{0}</a>: {1}/{2}, '
                        'target {3}/{4},<br/>'
                        '{5:.2f}% success, '
                        '{6:.2f}% total</td>'
                    ''.format(
                        pile[row][0], data['success'], data['total'],
                        data['target-success'],data['target-total'],
                        100*float(data['success'])/data['target-success'],
                        100*float(data['total'])/data['target-total'],
                    ))
            except IndexError:
                # Piles have different length, just catch and print an
                # empty cell.
                print('<td></td>')
        print('</tr>')
    print('</table>')
    end_document()


def main():
    '''
    Parse $QUERY_STRING and display the correct page
    '''
    query = os.getenv('QUERY_STRING', '')
    if '@' in query and 'x' in query and '-' in query:
        info(query)
    elif query.startswith('probability-'):
        probability_table(query.split('-')[1])
    elif query == 'status':
        status()
    else:
        preamble('Please choose')
        end_document()


if __name__ == '__main__':
    main()
