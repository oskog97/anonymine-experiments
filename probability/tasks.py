'''
This file defines constants used by both add-tasks and web.py
'''


x = 2**(1./6)
sizes = [int(0.5 + 10*x**i) for i in range(-2, 23)]
# Tweak last value to nice round number
assert sizes[-3] == 101
sizes[-3] = 100


densities = [
     5.0,  7.5,
    10.0, 10.5, 11.0, 11.5, 12.0, 12.5,
    13.0, 13.5, 14.0, 14.5, 15.0, 15.5,
    16.0, 16.5, 17.0, 17.5, 18.0, 18.5,
    19.0, 19.5, 20.0, 20.5, 21.0, 21.5,
    22.0, 22.5, 23.0, 23.5, 24.0, 24.5,
    25.0, 25.5, 26.0, 26.5, 27.0, 27.5,
    30.0, 32.5, 35.0,
]


    # Size, max density
density_sweep_sizes = [
    (10, 35.0),
    (20, 27.5),
    (32, 26.0),
    (50, 22.0),
]

    # Density, max size
size_sweep_densities = [
    (10.0, 127),
    (15.0, 127),
    (20.0, 100),
    (22.0, 80),
]

# 1 is not needed.  Just a few sweeps of aspect ratio at the crossing points of
# density_sweep_sizes and size_sweep_densities.
ratios = [1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4]
ratio_densities = [12.5, 15.0, 17.5, 20.0, 22.0]
ratio_sizes = [16, 20, 25, 32, 40, 50, 63]

# Originally up to 27.5% @ 63x63
area_max_density = 27.5
area_max_size = 63
# Extend on really low sizes and densities.
extended_area_max_density = 17.5
extended_area_max_size = 13


sweep_target = (2000, 50000)
area_target = (200, 5000)
extended_area_target = (100, 1000)
aspect_ratio_target = (1000, 20000)


for size, _ in density_sweep_sizes:
    assert size in sizes

for density, _ in size_sweep_densities:
    assert density in densities

del size, density

