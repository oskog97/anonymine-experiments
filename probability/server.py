#!/usr/bin/env python

import os
import random
import socket
import sys
import threading
import time
import traceback

# Local file socket_textio.py
from socket_textio import EOF, readline, send


# Globals

pythons = ('CPython 2', 'CPython 3', 'PyPy 2', 'PyPy 3')
close_on_fork = []

try:
    cfg = eval(open('server.cfg').read())
except:
    raise RuntimeError('Cannot import configuration from server.cfg')

db_lock = threading.Lock()
try:
    db = eval(open('database').read())
except:
    db = {}


# Functions


def log(s):
    sys.stdout.write(time.strftime('[%Y-%m-%d %H:%M:%S] ') + s + '\n')
    sys.stdout.flush()


def verify_category_string(category):
    '''Raise an exception if `category` is not a valid category string.'''
    mines, rest = category.split('@')
    int(mines)
    size, field_type = rest.split('-')
    width, height = map(int, size.split('x'))
    assert field_type in ('moore', 'neumann', 'hex')


def category_priority(category):
    '''
    Use as `key` argument to `sort` method or `sorted` function.
    `key` in this function MUST be a key in `db`.
    '''
    total = db[category]['total']
    maxtotal = db[category]['target-maxtotal']
    success = db[category]['success']
    minsuccess = db[category]['target-minsuccess']
    # Get accurate high probability measurements:
    success = min(success, total - success)
    # 0 = stop
    if maxtotal == 0 or minsuccess == 0:
        return 0
    # Done.
    if success >= minsuccess or total >= maxtotal:
        return 0
    #
    a = float(minsuccess - success)/minsuccess
    b = float(maxtotal - total)/maxtotal
    # Add noise to not give same answer to multiple clients.
    c = random.random()
    return 30*a + 10*b + c


def admin_handler(clientsocket):
    '''
    Handle client authenticated as admin
    Functions:
        - Cause tasks to exist and set their target success (min)
          and target total (max)
        - sync: Save database to disk
        - return
    '''
    while True:
        command = readline(clientsocket)
        if command == 'sync':
            db_lock.acquire()
            f = open('database', 'w')
            f.write(repr(db))
            db_lock.release()
            f.flush()
            os.fsync(f.fileno())
            f.close()
            continue
        if command == 'return':
            return

        # Functions to test what's going on with the lock:
        if command == 'islocked':
            send(clientsocket, repr(db_lock.locked()) + '\n')
            continue
        if command == 'unlock':
            try:
                db_lock.release()
                send(clientsocket, 'DB lock released.\n')
            except Exception as e:
                send(clientsocket, repr(e) + '\n')
            continue
        if command == 'lock':
            try:
                db_lock.acquire()
                send(clientsocket, 'DB lock acquired. DO NOT RETURN.\n')
            except Exception as e:
                send(clientsocket, repr(e) + '\n')
            continue

        # Add/change categories to be tested
        try:
            category, min_success, max_attempts = command.split(' ')
            min_success = int(min_success)
            max_attempts = int(max_attempts)
            # Check value of `category`
            verify_category_string(category)
        except:
            send(clientsocket, '?\n')
            continue
        db_lock.acquire()
        if category not in db:
            db[category] = {
                'total': 0,
                'success': 0,
                'rule9bf-useful': 0,
                'time': {},
            }
        db[category]['target-maxtotal'] = max_attempts
        db[category]['target-minsuccess'] = min_success
        db_lock.release()


def worker_handler(clientsocket):
    '''
    Handle client authenticated as worker
    Functions:
        - get-task      Return a task to do
        - publish       Return the result of the task
        - return
    '''
    while True:
        command = readline(clientsocket)
        # This is where pretty much all EOFs happen.
        if command == 'get-task':
            # Lock database to ensure get-task will not break if there is
            # a half initialized new category.
            db_lock.acquire()
            keys = db.keys()
            db_lock.release()
            # Sort by most complete to least complete
            most_needed = sorted(keys, key=category_priority)[-1]
            if category_priority(most_needed) > 0:
                send(clientsocket, most_needed + '\n')
            else:
                send(clientsocket, 'idle\n')
            # Check that autosync is still working.
            try:
                if os.stat('database').st_mtime + 45 < time.time():
                    log('Restarting autosync!')
                    autosync()
            except Exception as e:
                log(repr(e))

        elif command.startswith('publish '):
            try:
                category = command.split(' ')[1]
                verify_category_string(category)
                assert category in db, "category not in database"
                # Read data
                result = {}
                while True:
                    line = readline(clientsocket)
                    if line == 'end':
                        break
                    key, value = line.split(':', 1)
                    result[key] = value.strip()
                # Booleans
                for key in ('success', 'rule9bf-useful'):
                    assert result[key] in ('yes', 'no')
                    result[key] = (result[key] == 'yes')
                # If time was measured, make sure we have all data:
                if 'time' in result:
                    float(result['time'])
                    assert result['python'] in pythons
                    assert 'machine' in result
                # Update data
                db_lock.acquire()
                db[category]['total'] += 1
                db[category]['success'] += result['success']
                db[category]['rule9bf-useful'] += result['rule9bf-useful']
                if 'time' in result:
                    pair = (result['machine'], result['python'])
                    if pair in db[category]['time']:
                        total, samples = db[category]['time'][pair]
                    else:
                        total, samples = 0.0, 0
                    total += float(result['time'])
                    samples += 1
                    db[category]['time'][pair] = (total, samples)
                db_lock.release()
            except Exception as e:
                try:
                    db_lock.release()
                except:
                    pass
                send(clientsocket, 'Error parsing data: {}\n'.format(repr(e)))

        elif command == 'return':
            return
        elif command == '':
            continue
        else:
            send(clientsocket, "unknown command\n")


def viewer_handler(clientsocket):
    '''
    Handle client authenticated as viewer
    Functions:
        - stat              Show all categories with success/total etc
        - stat <match>      Only show categories that contain <match>
        - lines <n>         Terminal size for pagination in 'stat'
        - lines all         No pagination in 'stat'
        - time <key>        Time data for catergory <key>
        - return
    '''
    lines = 24
    while True:
        command = readline(clientsocket)
        if command == 'return':
            return

        elif command.startswith('stat'):
            # Selection
            if ' ' in command:
                match = command.split(' ', 1)[1]
            else:
                match = ''
            # Listing
            line_counter = 0
            # Make copy, iterating will fail if size of `database` changes.
            # Lock database briefly to ensure there are no half-initialized
            # new categories.
            db_lock.acquire()
            keys = db.keys()
            db_lock.release()
            for category in keys:
                if match not in category:
                    continue
                line = ('{:20} {:5} {:6}: {}/{},'
                        '\trule9bf useful {} times\n'
                ).format(
                    category,
                    db[category]['target-minsuccess'],
                    db[category]['target-maxtotal'],
                    db[category]['success'],
                    db[category]['total'],
                    db[category]['rule9bf-useful'],
                )
                send(clientsocket, line)
                # Pagination
                line_counter += 1
                if lines != 'all':
                    if line_counter % (lines - 1) == 0:
                        send(clientsocket, 'more: ')
                        readline(clientsocket)

        elif command.startswith('lines '):
            try:
                lines = command.split(' ')[1]
                if lines != 'all':
                    lines = int(lines)
            except:
                send(clientsocket, '?\n')
                continue

        elif command.startswith('time '):
            try:
                category = command.split(' ')[1]
                verify_category_string(category)
            except:
                send(clientsocket, '?\n')
                continue
            send(clientsocket,
                 # Field length + separating space
                #'           N/A'
                 '     CPython 2  '
                 '     CPython 3  '
                 '        PyPy 2  '
                 '        PyPy 3  '
                 '  Machine\n'
            )
            machines = {}
            # '(BENCHMARK=1 for other reasons)' entries may be automatically
            # removed at any time by a different thread.  Lock database while
            # reading.
            db_lock.acquire()
            # Make a copy of the pairs to iterate over:
            pairs = list(db[category]['time'])
            for machine, python in pairs:
                # Remove abuse of BENCHMARK environment variable:
                if machine == '(BENCHMARK=1 for other reasons)':
                    del db[category]['time'][(machine, python)]
                    continue
                # All four Pythons MUST have values:
                if machine not in machines:
                    machines[machine] = {}
                    for x in pythons:
                        machines[machine][x] = '           N/A'
                # Add average time
                total, samples = db[category]['time'][(machine, python)]
                machines[machine][python] = '{:8.2f} {:>5}'.format(
                    total/samples,
                    '({})'.format(samples if samples < 1000 else '1k+')
                )
            db_lock.release()
            # Print
            for machine in sorted(machines):
                for python in pythons:
                    send(clientsocket, machines[machine][python] + '  ')
                send(clientsocket, '  ' + machine + '\n')

        elif command == '':
            continue
        else:
            send(clientsocket, 'Unknown command.\n')


def client_handler(clientsocket, address):
    '''
    Handle a connected client.

    Will read a single line and then connect to other function

    cfg['admin-pass']   ->  admin_handler
    cfg['worker-pass']  ->  worker_handler
    'view'              ->  viewer_handler
    'exit'              ->  close connection
    '''
    try:
        log('Connected from {}'.format(address))
        while True:
            password = readline(clientsocket)
            if password == cfg['admin-pass']:
                log('{} authenticated as admin'.format(address))
                admin_handler(clientsocket)
            elif password == cfg['worker-pass']:
                log('{} authenticated as worker'.format(address))
                worker_handler(clientsocket)
            elif password == 'view':
                log('{} authenticated as viewer'.format(address))
                viewer_handler(clientsocket)
            elif password == 'exit':
                break
            else:
                log('{} tried {}'.format(address, repr(password)))
                send(clientsocket, '?\n')
    except EOF:
        log('Disconnect (sudden) {}'.format(address))
        try:
            clientsocket.shutdown(0)
            clientsocket.close()
        except:
            pass
        return
    except Exception as e:
        exception = sys.exc_info()
        log('{} had exception {}'.format(address, repr(e)))
        log(''.join(traceback.format_exception(*exception)))
        # Make sure we don't leave the database locked.
        if db_lock.locked():
            # Still a TOCTOU, but the original exception has already been
            # logged, so whatever.
            db_lock.release()
    try:
        log('Disconnect {}'.format(address))
        clientsocket.shutdown(0)
        clientsocket.close()
    except:
        pass


def autosync():
    '''
    forks off a process that will connect to the server and
    periodically execute the 'sync' command.
    '''
    try:
        pid = os.fork()
        if pid:
            log('Autosync PID = {}'.format(pid))
            return
        for file_to_close in close_on_fork:
            file_to_close.close()
        server_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_conn.connect(('localhost', cfg['port']))
        server_conn.send(cfg['admin-pass'].encode('utf-8') + b'\n')
        while True:
            # Re-sync immediately on start up to set modified time on the
            # database file.
            server_conn.send(b'sync\n')
            for i in range(30):
                time.sleep(1)
                # Check once a second if the parent is still running.
                # If not, die.
                if os.getppid() == 1:
                    exit(0)
    except KeyboardInterrupt:
        exit(0)
    except:
        exception = sys.exc_info()
        traceback.print_exception(*exception)
        filename = 'autosync.' + time.strftime('%Y-%m-%d_%H:%M:%H:%S')
        logfile = open(filename, 'w')
        traceback.print_exception(*exception, file=logfile)
        logfile.close()
        exit(1)


def server():
    '''
    main function
    '''
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversocket.bind(('', cfg['port']))    # NOTE: If change, see `autosync`
    serversocket.listen(5)
    close_on_fork.append(serversocket)
    autosync()
    while True:
        clientsocket, addr = serversocket.accept()
        handler = threading.Thread(
            target=client_handler, args=(clientsocket, addr)
        )
        handler.daemon = True
        handler.start()


if __name__ == '__main__':
    try:
        server()
    except KeyboardInterrupt:
        pass
