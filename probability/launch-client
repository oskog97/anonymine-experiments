#!/bin/sh

# Run client.py with every available interpreter, forever.
# Usage:
#   ./launch-client
#   ./launch-client fast
#   ./launch-client i686
# "fast" uses PyPy only
# "i686" disables Python 3, useful on old CPUs (pre Pentium 4)


# Need to set up signal handler before any child process is launched.
# $pid is set by 'launch'.
forward_USR1 ()
{
    if [ -z "$pid" ]; then
        echo "launch-client not ready"
        return
    fi
    printf "$$:\t"
    # Check if the client has launched its child process.
    # Make sure there is always a space before ppid.
    if ps -o pid,ppid | grep -q " $pid"'$'; then
        kill -USR1 $pid
    else
        echo "$pid not ready for SIGUSR1"
    fi
}
trap forward_USR1 USR1
trap forward_USR1 INFO 2>&-


# Check things

if (grep -q "'machine': 'Test'" client.cfg); then
    echo "Edit client.cfg" >&2
    exit 1
fi

has_python ()
{
    python="$1"
    if ! ("$python" -V >/dev/null 2>/dev/null); then
        echo "There is no $python" >&2
        echo false
        return
    fi
    if ! ("$python" -c "import anonymine_solver" >/dev/null 2>/dev/null); then
        echo "$python can't import anonymine_solver" >&2
        echo "Run 'sudo $python symlinks install' after" >&2
        echo "installing Anonymine" >&2
        echo false
        return
    fi
    echo true
}

has_python2=$(has_python python2)
has_python3=$(has_python python3)
has_pypy=$(has_python pypy)
has_pypy3=$(has_python pypy3)


# Launch one command and wait for it to finish, don't wait for signals.
# Stores PID in $pid.
launch ()
{
    "$@" &
    pid=$!
    while kill -0 "$pid" 2>&-; do
        wait "$pid"
    done
}

# Options for faster clients:
if [ "$1" = "fast" ]; then
    has_python2=false
    has_python3=false
fi
if [ "$1" = "i686" ]; then
    # CPython 3 is slower, especially on old CPUs.
    # This option is useful if you don't have PyPy installed.
    # SSE2 is normally required for PyPy.
    has_python3=false
fi

# Loop through available interpreters
while true; do
    $has_python2 && launch python2 client.py
    $has_python3 && launch python3 client.py
    $has_pypy    && launch pypy    client.py
    $has_pypy3   && launch pypy3   client.py
done
