import os
import sys

def log(s):
    print(s)
    f = open(sys.argv[1], 'a')
    f.write(s + '\n')
    f.close()

while True:
    try:
        interpreter = '"{}"'.format(sys.executable)
        ret = os.system('{} worker.py 2>>{}'.format(interpreter, sys.argv[1]))
        log('Process exit: {}'.format(ret))
    except KeyboardInterrupt:
        log('KeyboardInterrupt')
