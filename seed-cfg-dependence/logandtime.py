#import os
import sys
import time
import traceback


def iso8601(unix_time=None):
    format = '%Y-%m-%d %H:%M:%S'
    if unix_time is None:
        return time.strftime(format)
    else:
        return time.strftime(format, time.localtime(unix_time))


def log(message_format, *args):
    try:
        formatted_message = message_format.format(*args)
    except Exception as err:
        formatted_message = 'OOPS: exception {}, format {}, args {}'.format(
            repr(err), repr(message_format), repr(args)
        )

    # Add vertical separation if needed
    if '\n' in formatted_message:
        sys.stderr.write('\n')
    sys.stderr.write('{} {}\n'.format(iso8601(), formatted_message))
    if '\n' in formatted_message:
        sys.stderr.write('\n')

    sys.stderr.flush()


def log_tb(sys_exc_info_retval):
    tb = traceback.format_exception(*sys_exc_info_retval)
    log('Error\n{}', ''.join(tb))
