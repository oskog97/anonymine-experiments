import os
from task import Task

class Data():
    def __init__(self, writeable=False, tty=None):
        '''
        `dict`-like object that is permanently saved to a file called 'data'.

        Leave `writetable=False` for read-only access.

        Set `tty=sys.stdout` or `tty=sys.stderr` to print loading progress
        to the terminal.
        '''
        self.data = {}
        self.writeable = writeable

        # Create empty data file if necessary
        if 'data' not in os.listdir('.') and self.writeable:
            f = open('data', 'w')
            f.close()

        i = -1      # Initialized for empty/non-existing data file
        for i, line in enumerate(open('data')):
            task, result = eval(line)
            self.data[task] = result
            if tty is not None and i % 1000 == 999:
                tty.write('\r{}'.format(i+1))
                tty.flush()
        if tty is not None:
            tty.write('\r{}\n'.format(i+1))
            tty.flush()

        if self.writeable:
            self.file = open('data', 'a')


    def __getitem__(self, key):
        return self.data[key]


    def __setitem__(self, key, value):
        if not self.writeable:
            raise RuntimeError('Data object is not writeable')
        self.data[key] = value
        line_tuple = (key, value)
        self.file.write(repr(line_tuple) + '\n')


    def sync(self):
        if self.writeable:
            self.file.flush()


    def __del__(self):
        if self.writeable:
            self.file.close()


    def __contains__(self, thing):
        return thing in self.data


    def __iter__(self):
        return self.data.__iter__()


    def __len__(self):
        return len(self.data)


    def __repr__(self):
        #return repr(self.data)
        return 'Data(writeable={})'.format(self.writeable)


    def __str__(self):
        return str(self.data)
