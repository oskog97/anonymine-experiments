#!/usr/bin/env python3

import multiprocessing
import os
import platform
import time


def find_python(version):
    path = []

    if os.name == 'nt':
        # Find locally installed PyPy on Windows
        for directory in ['..', '..\\..']:
            for entry in os.listdir(directory):
                if entry.startswith('pypy' + str(version)):
                    path.append(os.path.join(directory, entry))
        extension = '.exe'
    else:
        extension = ''

    # System path
    sys_path = os.getenv('PATH').split(os.path.pathsep)
    path.extend(sys_path)

    # Find PyPy
    for directory in path:
        if version == 2:
            executable = os.path.join(directory, 'pypy' + extension)
        else:
            executable = os.path.join(directory, 'pypy3' + extension)
        if os.path.isfile(executable):
            # Avoid spaces when possible
            if directory in sys_path:
                return os.path.basename(executable)
            else:
                return executable

    # Find Python
    for directory in path:
        executable = os.path.join(directory, 'python'+str(version)+extension)
        if os.path.isfile(executable):
            return 'python' + str(version)

    return None


def main():
    error = False

    python2 = find_python(2)
    if python2 is None:
        print('ERROR: No Python 2')
        error = True
    else:
        print('Python 2 is ' + repr(python2))

    python3 = find_python(3)
    if python3 is None:
        print('ERROR: No Python 3')
        error = True
    else:
        print('Python 3 is ' + repr(python3))

    if not os.path.isfile('cfg'):
        print('ERROR: cfg missing')
        error = True

    if not os.path.isdir('/tmp'):
        try:
            # Create the directory on Windows if possible
            if os.name != 'nt':
                raise Exception('Did not try to create it')
            os.mkdir('/tmp')
        except Exception as err:
            print('/tmp does not exist and could not be created: ' + repr(err))
            error = True

    node = platform.uname()[1]

    if os.name == 'nt':
        problems = '\' "^%$()[]{}<>&|;'
    if os.name == 'posix':
        problems = '\' "\\$()[]{}<>&|;'
    for ch in problems:
        if ch in node:
            print('ERROR: Problematic characters in node name')
            error = True
        if ch in python2:
            print('ERROR: Problematic characters in path to Python 2')
            error = True
        if ch in python3:
            print('ERROR: Problematic characters in path to Python 3')
            error = True

    if os.name not in ('posix', 'nt'):
        print('ERROR: Unknown operating system family')
        error = True

    if error:
        if os.name == 'nt':
            os.system('pause')
        exit(1)

    # Launch
    logfiles = []
    for cpu in range(multiprocessing.cpu_count()):
        python_version = 2 + cpu%2
        logfile = 'worker-{}-{:02}-py{}.log'.format(node, cpu+1, python_version)
        if python_version == 2:
            interpreter = python2
        else:
            interpreter = python3

        if os.name == 'nt':
            # PyPy 3 keeps mysteriosly dying on Windows since updating
            # to Anonymine 0.6.34.
            # Start it in an infinite loop and record the exit code.
            os.system('start /low /min {} windows.py {}'.format(
                      interpreter, logfile))
            logfiles.append(logfile)

        if os.name == 'posix':
            os.system('{} worker.py 2>>{} &'.format(interpreter, logfile))

    # Tail the log files
    time.sleep(1)
    if os.name == 'posix':
        os.system("tail -f worker-{}-*.log".format(node))


if __name__ == '__main__':
    main()
