class EOF(Exception):
    '''
    Connection closed by remote host.
    Raised by `readline`.
    '''
    pass


def readline(clientsocket):
    r'''
    Read one line from the other end and return it without line ending.
    Both '\n' and '\r\n' is supported.

    Raises the `EOF` exception if the other end has closed the connection.
    (Also in this module.)

    Example.
    Server: >>> send(client, 'test\n')
    Client: >>> x = readline(server)
    Client: >>> x
    Client: 'test'
    '''
    buf = b''
    while True:
        ch = clientsocket.recv(1)
        if not ch and not buf:
            raise EOF
        if not ch:
            break
        if ch == b'\n':
            break
        buf += ch
    return buf.rstrip(b'\r').decode('utf-8')


def send(clientsocket, s):
    '''
    Send string `s` to other end, UTF-8 encoded.
    No newlines are added.
    '''
    clientsocket.send(s.encode('utf-8'))
