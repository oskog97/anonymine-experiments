#!/usr/bin/env python3

import errno
import random
import select
import signal
import socket
import sys
import time


class Timeout(Exception):
    pass

def alarm_handler(*args):
    raise Timeout

signal.signal(signal.SIGALRM, alarm_handler)


# Python 2.7 compatibility
try:
    ConnectionResetError
except NameError:
    class ConnectionResetError(Exception):
        pass


from socket_textio import readline, send, EOF
from logandtime import iso8601, log, log_tb
from task import Task, TaskSerializer, SanitizationError
from data import Data

cfg = eval(open('cfg').read())


# NOTE: globals
print('Loading saved data ...')
# File-backed dict like object
tasks_done = Data(writeable=True, tty=sys.stdout)
tasks_todo = []
# Tasks that are currently being processed by clients
tasks_inflight = set()

# True:  Hopefully more smooth time estimations
# False: Better compression of data file
shuffle_tasks = False


def get_task(python_version):
    for index, task in enumerate(tasks_todo):
        if task.python_version == python_version:
            chosen_task = tasks_todo.pop(index)
            tasks_inflight.add(chosen_task)
            out_task = TaskSerializer()
            out_task.from_Task(chosen_task)
            out_task.timeout = cfg['timeout']
            return out_task.format()
    else:
        # Both 'stall' and 'quit' will result in the client disconnecting,
        # but stall will cause the client to reconnect after an extended
        # delay.
        # 'stall' is used as long as other clients are working on something
        # as they might disconnect unexpectedly.
        for task in tasks_inflight:
            if task.python_version == python_version:
                return 'stall'
        else:
            # Absolutely nothing left for this Python version
            return 'quit'


def add_data(client):
    '''
    Called by `server`
    '''
    python_version = client['python-version']
    taskser = TaskSerializer()
    taskser.parse(client['working-on'])
    task = taskser.to_Task(python_version)
    client['counter'] += 1
    result = readline(client['socket'])
    # Count timeouts AND sanity check result
    if result == '<timeout>':
        client['timeouts'] += 1
    else:
        assert len(result) == 32
        int(result, 16)

    # Store result in the disk-backed dict-like object
    tasks_done[task] = result
    tasks_inflight.remove(task)


def print_progress(start_time, start_tasks, clients, lastupdate=[time.time()]):
    spinner = '|/-\\'
    foreground_colors = [31, 33, 35, 36, 37]
    python_colors = {
        2:  '\x1b[33m',
        3:  '\x1b[37m',
    }

    now = time.time()
    tasks_remaining = len(tasks_todo)
    tasks_completed = start_tasks - tasks_remaining

    # max 25 fps
    if now - lastupdate[0] < 0.040:
        return
    lastupdate[0] = time.time()

    if tasks_completed:
        time_done = (now-start_time) * tasks_remaining/tasks_completed + now
        time_done = iso8601(time_done)
    else:
        time_done = '---'

    screen = []
    screen.append('\x1b[1;1H')
    screen.append('\x1b[0m\x1b[37m\x1b[40m')
    screen.append('Tasks remaining: {}\n'.format(tasks_remaining))
    screen.append('Tasks completed: {}\n'.format(len(tasks_done)))
    screen.append('Currently working on: {}\n'.format(len(tasks_inflight)))
    screen.append('Estimated finish time: {}\n'.format(time_done))
    screen.append('\n')

    screen.append('\x1b[1m')    # Everything with bold
    hosts = []
    host_totals = {}
    for fd in clients:
        client = clients[fd]
        address = '{:>15}:{:<5}'.format(*client['address'])

        host = client['address'][0]
        if host not in hosts:
            hosts.append(host)
            host_totals[host] = {'counter': 0, 'speed': 0.0, 'timeouts': 0}
            host_totals[host]['python-versions'] = {2: 0, 3: 0}
        color_index = hosts.index(host) % len(foreground_colors)
        line_color = '\x1b[{}m'.format(foreground_colors[color_index])
        screen.append(line_color)

        if not client['authenticated']:
            screen.append('{} .....\n'.format(address))
            continue

        try:
            speed = client['counter'] / (now - client['start-time']) * 3600
        except ZeroDivisionError:
            speed = -1.0

        host_totals[host]['counter'] += client['counter']
        host_totals[host]['speed'] += speed
        host_totals[host]['timeouts'] += client['timeouts']
        host_totals[host]['python-versions'][client['python-version']] += 1

        screen.append(
            '{} {}Python {}  {}{} {:8}  {:8.1f} fields/h  {:6} timeouts\n'
            .format(
                address,
                python_colors[client['python-version']],
                client['python-version'],
                line_color,
                spinner[client['counter'] % len(spinner)],
                client['counter'],
                speed,
                client['timeouts']
            )
        )

    screen.append('\n\x1b[22m\x1b[37mHost totals:\n')
    screen.append('\x1b[1m')    # Everything with bold
    total_counter = 0
    total_speed = 0
    total_timeouts = 0
    total_python2 = 0
    total_python3 = 0
    for host in hosts:
        total_counter += host_totals[host]['counter']
        total_speed += host_totals[host]['speed']
        total_timeouts += host_totals[host]['timeouts']
        total_python2 += host_totals[host]['python-versions'][2]
        total_python3 += host_totals[host]['python-versions'][3]

        # Match color
        color_index = hosts.index(host) % len(foreground_colors)
        line_color = '\x1b[{}m'.format(foreground_colors[color_index])

        screen.append(
            '{}{:>15}       \x1b[33m{:3}  \x1b[37m{:3}  '
            '{}{} {:8}  {:8.1f} fields/h  {:6} timeouts\n'
            .format(
                line_color,
                host,
                host_totals[host]['python-versions'][2],
                host_totals[host]['python-versions'][3],
                #
                line_color,
                spinner[host_totals[host]['counter'] % len(spinner)],
                host_totals[host]['counter'],
                host_totals[host]['speed'],
                host_totals[host]['timeouts']
            )
        )

    screen.append('\n\x1b[22m\x1b[37mTotal:\n')
    screen.append(
        '{:>15}       {:3}  {:3}  {} {:8}  {:8.1f} fields/h  {:6} timeouts\n'
        .format(
            '',
            total_python2,
            total_python3,
            spinner[total_counter % len(spinner)],
            total_counter,
            total_speed,
            total_timeouts,
        )
    )

    sys.stdout.write(''.join(screen).replace('\n', '\x1b[K\n') + '\x1b[J')
    sys.stdout.flush()


def try_authenticate(clients_dict, fd, poll_object):
    client = clients_dict[fd]
    try:
        password_attempt = readline(client['socket'])
        if password_attempt != cfg['password']:
            raise ValueError
        python_version = int(readline(client['socket']))
        client['authenticated'] = True
        client['python-version'] = python_version
        return True
    except Exception:
        log('bad client {}', '{}:{}'.format(*client['address']))
        remove_client(clients_dict, fd, poll_object)
        return False


def remove_client(clients_dict, fd, poll_object):
    # Close
    connection = clients_dict[fd]['socket']
    # FIXME: This is duplicated in worker.py:
    try:
        try:
            connection.shutdown(socket.SHUT_RDWR)
        except socket.error as err:
            if err.errno != errno.ENOTCONN:
                raise
        except OSError as err:
            if err.errno != errno.ENOTCONN:
                raise
        connection.close()
    except Exception as err:
        tb = sys.exc_info()
        log('Unexpected exception closing connection: {}', repr(err))
        log_tb(tb)
    # Stop polling
    poll_object.unregister(fd)
    # Recover task if needed
    try:
        task_str = clients_dict[fd]['working-on']
        if not task_str in (None, 'quit', 'stall'):
            # Create Task object from internet safe string
            python_version = clients_dict[fd]['python-version']
            taskser = TaskSerializer()
            taskser.parse(task_str)
            task = taskser.to_Task(python_version)
            # Recover if needed
            if task not in tasks_done:
                tasks_todo.insert(0, task)
                tasks_inflight.remove(task)
                log(
                    'Recover lost task from {}',
                    '{}:{}'.format(*clients_dict[fd]['address'])
                )
    except Exception:
        log('Error attempting to recover lost task')
        log_tb(sys.exc_info())
    # Remove
    del clients_dict[fd]


def server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('', cfg['port']))
    server_socket.listen(5)
    poll = select.poll()
    server_socket_fd = server_socket.fileno()
    poll.register(server_socket_fd)

    start_time = time.time()
    start_tasks = len(tasks_todo)
    clients = {}

    # TODO: Handler TimeoutError (errno=110)

    while True:
        # FIXME:
        # Things here are kind of in the wrong order, because the main thing
        # is too big and should probably be in a different function.

        print_progress(start_time, start_tasks, clients)
        tasks_done.sync()   # Make sure data is on disk

        # Remove stale connections
        now = time.time()
        fd_list = list(clients.keys())
        for client_fd in fd_list:
            if now - clients[client_fd]['last-seen'] > cfg['max-timeout']:
                addr = clients[client_fd]['address']
                log('Removing stale connection {}:{}', *addr)
                remove_client(clients, client_fd, poll)

        # Poll all the connections
        for fd, eventmask in poll.poll(10000):
            try:
                signal.alarm(2)

                # New connection
                if fd == server_socket_fd:
                    client_socket, addr = server_socket.accept()
                    connect_time = time.time()
                    clients[client_socket.fileno()] = {
                        'socket': client_socket,
                        'address': addr,
                        'authenticated': False,
                        'python-version': None,
                        'working-on': None,
                        'counter': 0,
                        'timeouts': 0,
                        'start-time': connect_time,
                        'last-seen': connect_time,
                    }
                    ## POLLRDHUP not supported on PyPy
                    #events = select.POLLIN|select.POLLRDHUP
                    events = select.POLLIN
                    poll.register(client_socket, events)
                    continue

                # Client I/O
                client = clients[fd]
                client['last-seen'] = time.time()

                ## POLLRDHUP not supported on PyPy
                ## Disconnected client
                #if eventmask & select.POLLRDHUP:
                    #remove_client(clients, fd, poll)
                    #continue

                # Read from client
                try:
                    if not client['authenticated']:
                        if not try_authenticate(clients, fd, poll):
                            continue
                    if client['working-on'] is not None:
                        add_data(client)
                except EOF:
                    remove_client(clients, fd, poll)
                    continue
                except ConnectionResetError:
                    remove_client(clients, fd, poll)
                    continue

                # Write back to client
                try:
                    new_task = get_task(client['python-version'])
                    client['working-on'] = new_task
                    send(client['socket'], new_task + '\n')
                    if new_task in ('quit', 'stall'):
                        remove_client(clients, fd, poll)
                except ConnectionResetError:
                    remove_client(clients, fd, poll)

            except Exception:
                log_tb(sys.exc_info())
                try:
                    log('Offending client: {}', clients[fd]['address'])
                except KeyError:
                    pass
                # Remove offending client
                if fd in clients:
                    try:
                        remove_client(clients, fd, poll)
                    except Exception:
                        log_tb(sys.exc_info())
            finally:
                signal.alarm(0)


def tasks_list():
    tasks = []

    seeds = 1000
    side_range = (10, 15, 20, 25)
    density_range = (.10, .15, .20, .25, .30)
    neumann_mines_limit = 100
    stuck_check_range = range(1, 11)
    stuck_check_range_low = (1, 2, 3, 5, 10)
    rule9bf_max_range = range(0, 17)
    rule9bf_max_range_low = (0, 2, 4, 11, 13, 15)
    range_low_seeds = 100

    for seed in range(seeds):
        for python_version in (2, 3):
            for field_type in ('moore', 'hex', 'neumann'):
                for side in side_range:
                    for density in density_range:
                        mines = int(side**2 * density + .5)
                        if field_type == 'neumann':
                            if mines > neumann_mines_limit:
                                continue
                        for stuck_check in stuck_check_range:
                            for rule9bf_max in rule9bf_max_range:
                                if seed >= range_low_seeds:
                                    if stuck_check not in stuck_check_range_low:
                                        continue
                                    if rule9bf_max not in rule9bf_max_range_low:
                                        continue
                                tasks.append(Task(
                                    python_version=python_version,
                                    mines=mines,
                                    width=side,
                                    height=side,
                                    field_type=field_type,
                                    stuck_check=stuck_check,
                                    rule9bf_max=rule9bf_max,
                                    seed=seed
                                ))
        sys.stdout.write('\rInitialized: {}/{} ...'.format(seed + 1, seeds))
        sys.stdout.flush()
    return tasks


def main():
    for task in tasks_list():
        if task not in tasks_done:
            tasks_todo.append(task)
    if shuffle_tasks:
        print('\nShuffling task list ...')
        random.shuffle(tasks_todo)
    sys.stdout.write('\x1b[1;1H\x1b[J')
    server()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stdout.write('\x1b[0m\n')
