import collections

Task = collections.namedtuple('Task', [
    'python_version',
    'mines',
    'width',
    'height',
    'field_type',
    'stuck_check',
    'rule9bf_max',
    'seed',
])


class SanitizationError(Exception):
    pass


class TaskSerializer():
    def __init__(self, **kwargs):
        self.mines = None
        self.width = None
        self.height = None
        self.field_type = None
        self.stuck_check = None
        self.rule9bf_max = None
        self.timeout = None
        self.seed = None

        for key in kwargs:
            exec('self.{} = {}'.format(key, repr(kwargs[key])))


    def from_Task(self, task):
        self.mines = task.mines
        self.width = task.width
        self.height = task.height
        self.field_type = task.field_type
        self.stuck_check = task.stuck_check
        self.rule9bf_max = task.rule9bf_max
        self.seed = task.seed
        # self.timeout not populated


    def to_Task(self, python_version):
        self.sanitize()
        return Task(
            python_version=python_version,
            mines=self.mines,
            width=self.width,
            height=self.height,
            field_type=self.field_type,
            stuck_check=self.stuck_check,
            rule9bf_max=self.rule9bf_max,
            seed=self.seed
        )


    def sanitize(self):
        if self.timeout is None:
            raise SanitizationError("timeout not set")
        try:
            assert self.mines > 0 and self.mines < self.width * self.height
            assert self.width >= 4 and self.height >= 4
            assert self.field_type in ('moore', 'hex', 'neumann')
            assert self.stuck_check >= 0
            assert self.rule9bf_max >= 0
            assert self.timeout >= 1
            if True:
                assert self.width <= 200 and self.height <= 200
                assert self.stuck_check <= 20
                assert self.rule9bf_max <= 20       # 1048576
                assert self.timeout <= 86400
        except Exception as error:
            raise SanitizationError(repr(error))


    def parse(self, line):
        fields = line.split(':')
        if len(fields) > 8:
            raise SanitizationError('Too many items in ' + repr(line))
        if len(fields) < 8:
            raise SanitizationError('Too few items in ' + repr(line))
        self.mines          = int(fields[0])
        self.width          = int(fields[1])
        self.height         = int(fields[2])
        self.field_type     = fields[3]
        self.stuck_check    = int(fields[4])
        self.rule9bf_max    = int(fields[5])
        self.timeout        = int(fields[6])
        self.seed           = int(fields[7])
        try:
            self.sanitize()
        except SanitizationError as error:
            raise SanitizationError('Other error in {}: {}'.format(
                repr(line),
                str(error)
            ))


    def format(self):
        self.sanitize()
        return ':'.join(['{}']*8).format(
            self.mines,
            self.width,
            self.height,
            self.field_type,
            self.stuck_check,
            self.rule9bf_max,
            self.timeout,
            self.seed
        )

