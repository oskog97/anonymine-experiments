#!/usr/bin/env python3

import errno
import gc
import os
import platform
import signal
import socket
import subprocess
import sys
import threading
import time


# Python 2.7 compatibility
try:
    ConnectionRefusedError
except NameError:
    class ConnectionRefusedError(Exception):
        pass

try:
    BrokenPipeError
except NameError:
    class BrokenPipeError(Exception):
        pass


from socket_textio import readline, send, EOF
from task import TaskSerializer, Task
from logandtime import iso8601, log, log_tb

cfg = eval(open('cfg').read())

# *nix: Anonymine 0.6.33 or newer required.
# Windows 10+: Anonymine 0.6.34 or newer required.
sys.path.insert(0, cfg['anonymine-src-dir'])
import anonymine_engine
import test


class Stall(Exception):
    pass


def do_tasks(connection):
    while True:
        line = readline(connection)

        if line == 'quit':
            log("Received 'quit'")
            return

        # This is done as an exception that causes a reconnect
        # because it's easy and backward-compatible with currently
        # running clients.  (Old ones raise an exception in task.parse)
        if line == 'stall':
            log("Received 'stall'")
            raise Stall

        task = TaskSerializer()
        task.parse(line)

        anonymine_cfg_override = {
            'init-field': {
                'sec-maxtime':          task.timeout * cfg['timeout-scale'],
                'sec-maxarea':          200 * 200,
                'options': {
                    'nice':             19,
                    'stuck-check':      task.stuck_check,
                    'rule9bf-max':      task.rule9bf_max,
                    'rule9bf-action':   'deny',
                },
            }
        }
        parameters = {
            'mines':                    task.mines,
            'width':                    task.width,
            'height':                   task.height,
            'gametype':                 task.field_type,
            'guessless':                True,
            'seed':                     task.seed,
        }
        start_point =                   (0, 0)

        pid = os.getpid()

        try:
            hash = test._game_hash(
                anonymine_cfg_override, parameters, start_point
            )
        except anonymine_engine.security_alert:
            hash = '<timeout>'

        if os.getpid() != pid:
            log('Child {} executing in parent {}', os.getpid(), pid)
            exit(1)

        send(connection, hash + '\n')

        # Maybe PyPy will behave better???
        gc.collect()


def close_connection(connection):
    # FIXME: This is duplicated in controller.py:
    try:
        try:
            connection.shutdown(socket.SHUT_RDWR)
        except socket.error as err:
            if err.errno != errno.ENOTCONN:
                raise
        except OSError as err:
            if err.errno != errno.ENOTCONN:
                raise
        connection.close()
    except Exception as err:
        tb = sys.exc_info()
        log('Unexpected exception closing connection: {}', repr(err))
        log_tb(tb)


def main():
    if 'nice' in dir(os):
        os.nice(cfg['nice'])

    while True:
        try:
            connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            connection.connect((cfg['server'], cfg['port']))
            log(
                'Started, connection: {}, PID: {}',
                connection.getsockname(), os.getpid()
            )
            # Authenticate and inform the controller about Python version
            send(connection, cfg['password'] + '\n')
            if platform.python_implementation() in ('CPython', 'PyPy'):
                version = sys.version_info[0]
            else:
                raise BaseException('Incompatible Python interpreter')
            send(connection, str(version) + '\n')
            # All work is done in do_tasks
            do_tasks(connection)
            log('Complete')
            break
        except ConnectionRefusedError:
            log('Connection refused')
        except BrokenPipeError:
            log('Broken pipe')
        except socket.error as err:
            if err.errno == errno.ECONNREFUSED:
                log('Connection refused')
            elif err.errno == errno.EPIPE:
                log('Broken pipe')
            else:
                log_tb(sys.exc_info())
        except EOF:
            log('EOF on recv')
        except Stall:
            # Server wants us to reconnect after a few seconds delay
            time.sleep(10)
        except Exception:
            log_tb(sys.exc_info())
        except KeyboardInterrupt:
            break
        finally:
            close_connection(connection)
            time.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except BaseException as e:
        if type(e) not in (SystemExit, KeyboardInterrupt):
            log_tb(sys.exc_info())
            raise
        elif type(e) == KeyboardInterrupt:
            exit(1)
        elif type(e) == SystemExit:
            exit(e.code)
        assert False, "Not reached"
